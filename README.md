# Alpine image with OpenSSH client, LFTP and Git preinstalled

## Usage

### Setting up SSH keys

For example, within a GitLab-CI script section (see https://gitlab.com/gitlab-examples/ssh-private-key/-/blob/master/.gitlab-ci.yml for more infos):

```shell
# Start SSH agent session
eval $(ssh-agent -s)

# Add the SSH key stored in a variable to the agent store
# Note: we assume the key has been previously encoded in BASE64
cat $SSH_PRIVATE_KEY_BASE64 | base64 -d | ssh-add -

# SSH-keyscan keys of servers to connect to. 
ssh-keyscan "$HOST" >> ~/.ssh/known_hosts

# Or write them directly... 
# echo "$SSH_SERVER_HOSTKEYS" > ~/.ssh/known_hosts'

chmod 644 ~/.ssh/known_hosts
```

### Setting up Git user

Optionally, set the user name and email to use with Git.   

```shell
git config --global user.email "darth@empire.com"
git config --global user.name "Darth Vader"

```
