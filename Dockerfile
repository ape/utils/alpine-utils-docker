FROM alpine

RUN mkdir ~/.ssh && chmod 700 ~/.ssh

RUN apk add --no-cache openssh-client lftp git curl
